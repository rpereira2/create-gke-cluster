# GKE cluster operations

## Create GKE cluster

Script to create a Kubernetes cluster in GKE and print cluster details needed to
connect it to GitLab.

Once the cluster is created, the script will print the values of:

1. Token
1. IP
1. CA certificate

You can use these values to connect the cluster to GitLab.

Note that this script will modify your kubeconfig so that kubectl connects to the
newly created cluster.

### Prerequisites

1. You need to have [gcloud](https://cloud.google.com/sdk/docs/) installed and
    [initialized](https://cloud.google.com/sdk/docs/quickstarts).
1. Make sure there is a default project set by inspecting the output of `gcloud config list`.

    You can set the default project by executing

    ```bash
    gcloud config set project monitoring-development-241420
    ```

    where 'monitoring-development-241420' is a project ID.

    It is also useful (not mandatory) to set a default zone.
    You can set the default region and zone with:

    ```bash
    gcloud config set compute/region us-central1
    gcloud config set compute/zone us-central1-a
    ```

    If you do not set a default zone, you will need to pass in a `--zone us-central1-a`
    argument to the script.

1. You also need to have [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
installed.

### Usage

The only mandatory argument is the name of the cluster.

Calling the script with only the name of the cluster will create a *single node* (n1-standard-1)
cluster in your default project and zone.

```bash
./create_cluster.sh rpereira2-test1
```

In addition to the cluster name, you can pass in any arguments that are valid for the
[`gcloud container clusters create`](https://cloud.google.com/sdk/gcloud/reference/container/clusters/create)
command.

1. To create a cluster with 2 nodes:

    ```bash
    ./create_cluster.sh rpereira2-test1 --num-nodes 2
    ```

1. To create a cluster in a zone different from your default

    ```bash
    ./create_cluster.sh rpereira2-test1 --zone asia-south1-a
    ```

1. To create a "regional" cluster with 2 nodes in each zone of the region

    ```bash
    ./create_cluster.sh rpereira2-test1 --region us-central1 --num-nodes 2
    ```

1. To create a cluster with a specific kind of node:

    ```bash
    ./create_cluster.sh rpereira2-test1 --machine-type='g1-small'
    ```

## Delete GKE cluster

Deleting a GKE cluster from the Google Cloud console or the gcloud CLI does not
delete any persistent disk volumes or external load balancers (https://cloud.google.com/kubernetes-engine/docs/how-to/deleting-a-cluster).

This script will list any persistent disk volumes attached to the cluster and delete
both the cluster and its persistent disk volumes.

### Usage

The only mandatory argument is the name of the cluster.

```bash
./delete_cluster.sh rpereira2-test1
```

Other than the cluster name, any other arguments you pass will be passed to the
`gcloud container clusters get-credentials`, `gcloud container clusters delete` and
`gcloud compute disks delete` commands.

For example, to delete a cluster which was created in a different zone:

```bash
./delete_cluster.sh rpereira2-test1 --zone us-central1
```
